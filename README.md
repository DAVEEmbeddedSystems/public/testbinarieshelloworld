# TestBinariesHelloWorld

The purpose of this repository is to track the C/C++ sources, in order to printing the `Hello World!` message on target.

First you need to perform the source the environment on your VM, so you can compile the sources files for your target. Subsequently the binaries obtained must be copied to the target, for example vi `scp`, and then run them.

## Example

On MVM side

```bash
git clone https://gitlab.com/DAVEEmbeddedSystems/public/helloworldtestapps.git
source /home/dvdk/desk-mx-l/desk-mx6-l-4.0.0_env.sh
make C_helloworld
make CPP_helloworld
scp C_helloworld CPP_helloworld root@<ip your target>:/home/root/.
```

On target side

```bash
./C_helloworld
./CPP_helloworld
```
