
CFLAGS= -lstdc++
ODIR=obj
SDIR=src

_OBJ = C_helloworld.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

_OBJCPP = CPP_helloworld.o
OBJCPP = $(patsubst %,$(ODIR)/%,$(_OBJCPP))

_SRC = C_helloworld.c
SRC = $(patsubst %,$(SDIR)/%,$(_SRC))

_SRCCPP = C_helloworld.cpp
SRCCPP = $(patsubst %,$(SDIR)/%,$(_SRCCPP))

$(ODIR)/%.o: $(SDIR)/%.c
	$(CC) -c -o $@ $<

$(ODIR)/%.o: $(SDIR)/%.cpp
	$(CC) -c -o $@ $<

C_helloworld: $(OBJ)
	$(CC) -o $@ $^

CPP_helloworld: $(OBJCPP)
	$(CC) -o $@ $^ $(CFLAGS)

.PHONY: clean

clean:
	rm -f $(ODIR)/*.o *~
	rm C_helloworld
	rm CPP_helloworld
